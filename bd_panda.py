
import pandas as pd
import requests
import json


def get_header(csrf_token, bearer_token):
    return {
        'X-CSRF-TOKEN': csrf_token,
        'Authorization': 'Bearer {}'.format(bearer_token),
        'Accept': 'application/vnd.blackducksoftware.project-detail-4+json',
        'Content-Type': 'application/json'}


def get_auth_token(baseurl, api_token):
    authendpoint = baseurl + "/api/tokens/authenticate"
    session = requests.session()
    response = session.post(authendpoint, data={}, headers={'Authorization': 'token {}'.format(api_token)})
    csrf_token = response.headers['X-CSRF-TOKEN']
    try:
        bearer_token = json.loads(response.content.decode('utf-8'))['bearerToken']
        return csrf_token, bearer_token
    except:
        print("Failed to obtain bearer token, check for valid authentication token")
        exit(1)


def get_project_by_name(headers1, baseurl, project_name):
    project_url = baseurl + "/api/projects?q=name%3A" + project_name
    response = requests.get(project_url, headers=headers1)
    project_list = response.json()
    for project in project_list['items']:
        if project['name'] == project_name:
            return project


def get_version_by_name(headers1, project_name, version_name):
    version_url = project['_meta']['href'] + "/versions?q=versionName%3A" + version_name

    response = requests.get(version_url, headers=headers1)
    version_list = response.json()
    for version in version_list['items']:
        if version['versionName'] == version_name:
            return version


def get_vuln_count_impactwise(headers, version_url):
    headers['Accept'] = 'application/json'
    vuln_url = version_url + "/components"
    responseVul = requests.get(vuln_url, headers=headers)
    df = pd.DataFrame(responseVul.json()['items'])
    print(df)



url="https://testing.blackduck.synopsys.com"
token="MTdhMWRmNGUtMWQyYS00NGZiLWFkZjEtYTZkZTBjNjY5NjFjOjgxN2NiYjQ3LTAzZjktNDE0MS04MzNhLTBmMGEwMmEyOTQ0Mg=="
project="pavan_linux_java"
version="Release"

csrf_token, bearer_token = get_auth_token(url, token)
header = get_header(csrf_token, bearer_token)
project = get_project_by_name(header, url, project)
if project:
    version = get_version_by_name(header, project, version)
    if version:
        get_vuln_count_impactwise(header, version['_meta']['href'])
    else:
        print("No valid version found ")
else:
    print("No valid project found ")

